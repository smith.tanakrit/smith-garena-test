package Controllers

import (
	"github.com/gin-gonic/gin"
	"go-api/Models"
	"net/http"
)

func GetCountry(c *gin.Context) {
	var countries []Models.Country
	var err error

	limit, hasLimit := c.GetQuery("limit")
	filters, _ := c.GetQueryMap("filters")

	if hasLimit {
		err = Models.GetAll(&countries, limit, filters)
	} else {
		err = Models.GetAll(&countries, "0", filters)
	}

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "NOT_FOUND",
			"data":    []int{},
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
			"data":    countries,
		})
	}
}

func GetCountryByID(c *gin.Context) {
	param := c.Params.ByName("param")
	var country Models.Country
	err := Models.FindWhere(&country, "id", param)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "NOT_FOUND",
			"data":    []int{},
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
			"data":    country,
		})
	}
}

func GetCountryBySlug(c *gin.Context) {
	param := c.Params.ByName("param")
	var country Models.Country
	err := Models.FindWhere(&country, "slug", param)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "NOT_FOUND",
			"data":    []int{},
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
			"data":    country,
		})
	}
}
