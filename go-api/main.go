package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"go-api/Config"
	"go-api/Routes"
)

var err error

func main() {
	Config.DB, err = gorm.Open("mysql", Config.DbURL(Config.BuildDBConfig()))

	if err != nil {
		fmt.Println("Status:", err)
	}

	defer Config.DB.Close()

	r := Routes.SetupRouter()
	r.Run(":5000")
}
