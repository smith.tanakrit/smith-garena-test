package Models

import (
	"encoding/json"
	"time"
)

type Country struct {
	Id            uint            `json:"id"`
	Name          string          `json:"name"`
	Alpha2Code    string          `json:"alpha2Code" gorm:"column:alpha2Code"`
	Alpha3Code    string          `json:"alpha3Code" gorm:"column:alpha3Code"`
	Slug          string          `json:"slug"`
	CallingCodes  json.RawMessage `json:"callingCodes" gorm:"column:callingCodes"`
	Capital       string          `json:"capital"`
	AltSpellings  json.RawMessage `json:"altSpellings" gorm:"column:altSpellings"`
	Region        string          `json:"region"`
	Subregion     string          `json:"subregion"`
	Population    string          `json:"population"`
	Latitude      string          `json:"latitude"`
	Longitude     string          `json:"longitude"`
	Demonym       string          `json:"demonym"`
	Area          string          `json:"area"`
	Gini          string          `json:"gini"`
	Timezones     json.RawMessage `json:"timezones"`
	Borders       json.RawMessage `json:"borders"`
	NativeName    string          `json:"nativeName" gorm:"column:nativeName"`
	NumericCode   string          `json:"numericCode" gorm:"column:numericCode"`
	Currencies    json.RawMessage `json:"currencies"`
	Languages     json.RawMessage `json:"languages"`
	Translations  json.RawMessage `json:"translations"`
	Flag          string          `json:"flag"`
	RegionalBlocs json.RawMessage `json:"regionalBlocs" gorm:"column:regionalBlocs"`
	Cioc          string          `json:"cioc"`
	Created_at    time.Time       `json:"created_at"`
	Updated_at    time.Time       `json:"updated_at"`
}

func (country *Country) TableName() string {
	return "Countries"
}
