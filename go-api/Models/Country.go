package Models

import (
	_ "github.com/go-sql-driver/mysql"
	"go-api/Config"
)

func GetAll(countries *[]Country, limit string, filters map[string]string) (err error) {
	query := Config.DB

	if minPopulation, found := filters["min_population"]; found {
		query = query.Where("population >= ?", minPopulation)
	}

	if maxPopulation, found := filters["max_population"]; found {
		query = query.Where("population <= ?", maxPopulation)
	}

	if limit != "0" {
		query = query.Limit(limit)
	}

	err = query.Find(countries).Error

	if err != nil {
		return err
	}

	return nil
}

func FindWhere(country *Country, column string, value string) (err error) {
	if err = Config.DB.Where(column+" = ?", value).First(country).Error; err != nil {
		return err
	}

	return nil
}
