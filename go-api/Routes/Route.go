package Routes

import (
	"github.com/gin-gonic/gin"
	"go-api/Controllers"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	api := r.Group("go-api")
	{
		countries := api.Group("countries")
		{
			countries.GET("/", Controllers.GetCountry)
			countries.GET(":param", Controllers.GetCountryByID)
			countries.GET(":param/slug", Controllers.GetCountryBySlug)
		}
	}
	return r
}
