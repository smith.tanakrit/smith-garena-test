<h1 align="center">Smith Garena Test</h1>

### A project directory tree
    .
    ├── docker                              # Docker compiled file (included database data).
    ├── frontend                            # main web page (VueJs).
    ├── go-api                              # Golang API to provide country data to frontend.
    ├── src                                 # Laravel command for generate PDF and call third party API to update data to mysql database.
    ├── nginx                               # Nginx config file.
    ├── docker-compose.yml                  # Docker compose file to setup the container.
    ├── Dockerfile                          # Docker script for container.
    ├── start-smith-garena-test-go-api.sh   # A shell command to build and run Golang api when container is up.
    ├── start-smith-garena-test-web.sh      # A shell command to run nginx and php when container is up.
    └── README.md

## วิธีการรัน
- ทำการ set host ที่เครื่อง โดยการเพิ่ม 127.0.0.1 smith.garena.test ที่ /etc/hosts ในเครื่อง
- เข้ามาที่โปรเจค cd .../smith-garena-test
- build docker โดยคำสั่ง docker-compose up -d (ครั้งแรกใช้อาจเวลานาน)
- เมื่อทั้ง 3 container ด้านล่างนี้ ขึ้น done อาจรอสัก 5 - 10 วินาทีเพื่อให้ Shell script run ให้สมบูรณ์
  - smith_garena_test_mysql, 
  - smith_garena_test_api,
  - smith_garena_testcontainer

- สามารถเข้าเว็บไซต์โปรเจคได้ที่ [https://smith.garena.test](https://smith.garena.test) เนื่องจาก domain name ลงท้ายด้วย .test ทำให้ต้องใช้ SSL ซึ่งหากเข้าเว็บแล้วติด SSL หมดอายุ ให้กดแป้นพิมพ์ตามนี้ "thisisunsafe" ไม่ต้อง Enter ถ้าพิมพ์ผิดให้รีเฟรชแล้วพิมพ์ใหม่
  
## อธิบายโปรเจค
- เมื่อเข้าเว็บไซต์ ระบบจะ call API ไปที่ Golang ที่ออกแบบไว้ในรูปแบบของ RestfulAPI URL https://smith.garena.test/go-api/countries โดยส่ง paremeters limit=29&filters[min_population]=300000 ตามโจทย์ มาแสดงข้อมูลประเทศตามดีไซน์ *เพิ่มเติม (สามารถเปลี่ยน value ของ parameters ได้ รวมถึงมี filters max_population ด้วย และสามารถค้นหา ประเทศจาก slug ได้ผ่าน https://smith.garena.test/go-api/countries/*slug-ของประเทศ*/slug)
- ในหน้าเว็บไซต์หากกดที่ ปุ่ม World Covid Data PDF หรือกดที่ ตัวธง จะเป็นการ call API ไปหา Laravel เพื่อทำการดึง file PDF (ถ้ากดที่ปุ่ม World จะดึงไฟล์ data ทั้งโลกที่ถูก Genarate ไว้รออยู่แล้วจาก command ที่รันทุกๆ 1 ชั่วโมง แต่หากกดจากธงจะเป็นการ call Third party API และ Genarate PDF ใหม่ทันที ซึ่งอาจใช้เวลา 3-5 วินาที)
- ในส่วนของ Laravel command สามารถสั่ง run ได้ดังนี้ (ในการรันแต่ละครั้งอาจะใช้เวลาประมาณ 5-10 นาที)
  - เข้ามาที่โปรเจค cd .../smith-garena-test
  - Shell เข้าไปที่ docker ผ่าน docker exec -it smith_garena_test bash
  - จากนั้นเข้าไปที่ laravel ผ่าน cd /var/www/src
  - สั่ง run command ผ่าน php artisan fetch-country-list
  - ระบบจะทำการ call Third party API จาก https://restcountries.eu/ และ https://api.covid19api.com/countries เพื่อนำมา insert or update ลงบนฐานข้อมูล
    ซึ่งในขั้นตอนนี้ที่ต้องทำการ call ทั้ง 2 Third party API เนื่องจาก ในช่วงที่ระบบทำการ Genarate ไฟล์ ข้อมูล covid PDF ระบบจะทำการดึง covid data ผ่าน https://api.covid19api.com/total/country/*slug-ปรเทศ*/status/confirmed ต้องใช้ slug ที่มาจาก API https://api.covid19api.com/countries เท่านั้น ซึ่งจาก API https://restcountries.eu/ ไม่มีข้อมูล slug ที่ตรงกับของ API covid หรือถ้าระบบจะสร้าง slug เองจากชื่อประเทศ บางประเทศอาจมี slug ไม่ตรงกันกับที่ API covid รองรับ ส่งผลให้ไม่สามารถดึงข้อมูลได้
  - หลังจากที่ update ประเทศลงฐานข้อมูลสำเร็จ ระบบจะทำการสร้างไฟล์ PDF ดังที่กล่าวมาข้างต้น จากการ call Third party API เพื่อดึงข้อมูล covid data ในช่วง (เดือน มกราคม-พฤษภาคม ของปี 2020) ตามโจทย์ ซึ่งนอกจากจะสร้างไฟล์ covid-world-data แล้ว command นี้ยังสร้างไฟล์แยกของแต่ละประเทศเก็บไว้ด้วย (ตามที่พี่โค้กบอกในอีเมลครับ) ไฟล์ทั้งหมดจะอยู่ที่ smith-garena-test/src/storage/app/public/covid-data-pdf

<br>

**GIT Repo:** https://gitlab.com/smith.tanakrit/smith-garena-test 

**Tanakrit Phonsucharoen**

**Contact**
- Email: smith.tanakrit@gmail.com
- Tel: 082-469-4590
    

