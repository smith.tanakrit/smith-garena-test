<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique()->index();
            $table->string('alpha2Code', 5)->unique()->index();
            $table->string('alpha3Code', 5)->unique()->index();
            $table->string('slug')->unique()->index();
            $table->text('callingCodes')->nullable();
            $table->string('capital')->nullable();
            $table->text('altSpellings')->nullable();
            $table->string('region')->index()->nullable();
            $table->string('subregion')->index()->nullable();
            $table->unsignedBigInteger('population')->default(0);
            $table->string('latitude', 50)->nullable();
            $table->string('longitude', 50)->nullable();
            $table->string('demonym')->nullable();
            $table->decimal('area', 12, 2)->nullable();
            $table->decimal('gini', 12, 2)->nullable();
            $table->text('timezones')->nullable();
            $table->text('borders')->nullable();
            $table->string('nativeName')->nullable();
            $table->string('numericCode', 20)->nullable();
            $table->text('currencies')->nullable();
            $table->text('languages')->nullable();
            $table->text('translations')->nullable();
            $table->string('flag')->nullable();
            $table->text('regionalBlocs')->nullable();
            $table->string('cioc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
