<?php

return array(
    0 =>
        array(
            'Country' => 'Monaco',
            'Slug' => 'monaco',
            'ISO2' => 'MC',
        ),
    1 =>
        array(
            'Country' => 'Peru',
            'Slug' => 'peru',
            'ISO2' => 'PE',
        ),
    2 =>
        array(
            'Country' => 'Syrian Arab Republic (Syria)',
            'Slug' => 'syria',
            'ISO2' => 'SY',
        ),
    3 =>
        array(
            'Country' => 'Taiwan, Republic of China',
            'Slug' => 'taiwan',
            'ISO2' => 'TW',
        ),
    4 =>
        array(
            'Country' => 'Tonga',
            'Slug' => 'tonga',
            'ISO2' => 'TO',
        ),
    5 =>
        array(
            'Country' => 'France',
            'Slug' => 'france',
            'ISO2' => 'FR',
        ),
    6 =>
        array(
            'Country' => 'Guinea',
            'Slug' => 'guinea',
            'ISO2' => 'GN',
        ),
    7 =>
        array(
            'Country' => 'Maldives',
            'Slug' => 'maldives',
            'ISO2' => 'MV',
        ),
    8 =>
        array(
            'Country' => 'United States of America',
            'Slug' => 'united-states',
            'ISO2' => 'US',
        ),
    9 =>
        array(
            'Country' => 'Sao Tome and Principe',
            'Slug' => 'sao-tome-and-principe',
            'ISO2' => 'ST',
        ),
    10 =>
        array(
            'Country' => 'Bahrain',
            'Slug' => 'bahrain',
            'ISO2' => 'BH',
        ),
    11 =>
        array(
            'Country' => 'Guyana',
            'Slug' => 'guyana',
            'ISO2' => 'GY',
        ),
    12 =>
        array(
            'Country' => 'Saint Kitts and Nevis',
            'Slug' => 'saint-kitts-and-nevis',
            'ISO2' => 'KN',
        ),
    13 =>
        array(
            'Country' => 'Japan',
            'Slug' => 'japan',
            'ISO2' => 'JP',
        ),
    14 =>
        array(
            'Country' => 'Poland',
            'Slug' => 'poland',
            'ISO2' => 'PL',
        ),
    15 =>
        array(
            'Country' => 'Puerto Rico',
            'Slug' => 'puerto-rico',
            'ISO2' => 'PR',
        ),
    16 =>
        array(
            'Country' => 'Faroe Islands',
            'Slug' => 'faroe-islands',
            'ISO2' => 'FO',
        ),
    17 =>
        array(
            'Country' => 'Grenada',
            'Slug' => 'grenada',
            'ISO2' => 'GD',
        ),
    18 =>
        array(
            'Country' => 'Dominica',
            'Slug' => 'dominica',
            'ISO2' => 'DM',
        ),
    19 =>
        array(
            'Country' => 'Guadeloupe',
            'Slug' => 'guadeloupe',
            'ISO2' => 'GP',
        ),
    20 =>
        array(
            'Country' => 'India',
            'Slug' => 'india',
            'ISO2' => 'IN',
        ),
    21 =>
        array(
            'Country' => 'Netherlands Antilles',
            'Slug' => 'netherlands-antilles',
            'ISO2' => 'AN',
        ),
    22 =>
        array(
            'Country' => 'Sierra Leone',
            'Slug' => 'sierra-leone',
            'ISO2' => 'SL',
        ),
    23 =>
        array(
            'Country' => 'Afghanistan',
            'Slug' => 'afghanistan',
            'ISO2' => 'AF',
        ),
    24 =>
        array(
            'Country' => 'Chile',
            'Slug' => 'chile',
            'ISO2' => 'CL',
        ),
    25 =>
        array(
            'Country' => 'Gabon',
            'Slug' => 'gabon',
            'ISO2' => 'GA',
        ),
    26 =>
        array(
            'Country' => 'Ghana',
            'Slug' => 'ghana',
            'ISO2' => 'GH',
        ),
    27 =>
        array(
            'Country' => 'Korea (North)',
            'Slug' => 'korea-north',
            'ISO2' => 'KP',
        ),
    28 =>
        array(
            'Country' => 'Republic of Kosovo',
            'Slug' => 'kosovo',
            'ISO2' => 'XK',
        ),
    29 =>
        array(
            'Country' => 'Wallis and Futuna Islands',
            'Slug' => 'wallis-and-futuna-islands',
            'ISO2' => 'WF',
        ),
    30 =>
        array(
            'Country' => 'Comoros',
            'Slug' => 'comoros',
            'ISO2' => 'KM',
        ),
    31 =>
        array(
            'Country' => 'Kiribati',
            'Slug' => 'kiribati',
            'ISO2' => 'KI',
        ),
    32 =>
        array(
            'Country' => 'Western Sahara',
            'Slug' => 'western-sahara',
            'ISO2' => 'EH',
        ),
    33 =>
        array(
            'Country' => 'Cook Islands',
            'Slug' => 'cook-islands',
            'ISO2' => 'CK',
        ),
    34 =>
        array(
            'Country' => 'Gibraltar',
            'Slug' => 'gibraltar',
            'ISO2' => 'GI',
        ),
    35 =>
        array(
            'Country' => 'Singapore',
            'Slug' => 'singapore',
            'ISO2' => 'SG',
        ),
    36 =>
        array(
            'Country' => 'Côte d\'Ivoire',
            'Slug' => 'cote-divoire',
            'ISO2' => 'CI',
        ),
    37 =>
        array(
            'Country' => 'Malawi',
            'Slug' => 'malawi',
            'ISO2' => 'MW',
        ),
    38 =>
        array(
            'Country' => 'Slovenia',
            'Slug' => 'slovenia',
            'ISO2' => 'SI',
        ),
    39 =>
        array(
            'Country' => 'Hungary',
            'Slug' => 'hungary',
            'ISO2' => 'HU',
        ),
    40 =>
        array(
            'Country' => 'Iceland',
            'Slug' => 'iceland',
            'ISO2' => 'IS',
        ),
    41 =>
        array(
            'Country' => 'Sudan',
            'Slug' => 'sudan',
            'ISO2' => 'SD',
        ),
    42 =>
        array(
            'Country' => 'Ecuador',
            'Slug' => 'ecuador',
            'ISO2' => 'EC',
        ),
    43 =>
        array(
            'Country' => 'Falkland Islands (Malvinas)',
            'Slug' => 'falkland-islands-malvinas',
            'ISO2' => 'FK',
        ),
    44 =>
        array(
            'Country' => 'French Guiana',
            'Slug' => 'french-guiana',
            'ISO2' => 'GF',
        ),
    45 =>
        array(
            'Country' => 'Heard and Mcdonald Islands',
            'Slug' => 'heard-and-mcdonald-islands',
            'ISO2' => 'HM',
        ),
    46 =>
        array(
            'Country' => 'Solomon Islands',
            'Slug' => 'solomon-islands',
            'ISO2' => 'SB',
        ),
    47 =>
        array(
            'Country' => 'Austria',
            'Slug' => 'austria',
            'ISO2' => 'AT',
        ),
    48 =>
        array(
            'Country' => 'Finland',
            'Slug' => 'finland',
            'ISO2' => 'FI',
        ),
    49 =>
        array(
            'Country' => 'Bermuda',
            'Slug' => 'bermuda',
            'ISO2' => 'BM',
        ),
    50 =>
        array(
            'Country' => 'Greece',
            'Slug' => 'greece',
            'ISO2' => 'GR',
        ),
    51 =>
        array(
            'Country' => 'Latvia',
            'Slug' => 'latvia',
            'ISO2' => 'LV',
        ),
    52 =>
        array(
            'Country' => 'Armenia',
            'Slug' => 'armenia',
            'ISO2' => 'AM',
        ),
    53 =>
        array(
            'Country' => 'Saint Pierre and Miquelon',
            'Slug' => 'saint-pierre-and-miquelon',
            'ISO2' => 'PM',
        ),
    54 =>
        array(
            'Country' => 'Viet Nam',
            'Slug' => 'vietnam',
            'ISO2' => 'VN',
        ),
    55 =>
        array(
            'Country' => 'Madagascar',
            'Slug' => 'madagascar',
            'ISO2' => 'MG',
        ),
    56 =>
        array(
            'Country' => 'Panama',
            'Slug' => 'panama',
            'ISO2' => 'PA',
        ),
    57 =>
        array(
            'Country' => 'Cuba',
            'Slug' => 'cuba',
            'ISO2' => 'CU',
        ),
    58 =>
        array(
            'Country' => 'Martinique',
            'Slug' => 'martinique',
            'ISO2' => 'MQ',
        ),
    59 =>
        array(
            'Country' => 'Switzerland',
            'Slug' => 'switzerland',
            'ISO2' => 'CH',
        ),
    60 =>
        array(
            'Country' => 'Colombia',
            'Slug' => 'colombia',
            'ISO2' => 'CO',
        ),
    61 =>
        array(
            'Country' => 'Cape Verde',
            'Slug' => 'cape-verde',
            'ISO2' => 'CV',
        ),
    62 =>
        array(
            'Country' => 'China',
            'Slug' => 'china',
            'ISO2' => 'CN',
        ),
    63 =>
        array(
            'Country' => 'Guinea-Bissau',
            'Slug' => 'guinea-bissau',
            'ISO2' => 'GW',
        ),
    64 =>
        array(
            'Country' => 'Samoa',
            'Slug' => 'samoa',
            'ISO2' => 'WS',
        ),
    65 =>
        array(
            'Country' => 'ALA Aland Islands',
            'Slug' => 'ala-aland-islands',
            'ISO2' => 'AX',
        ),
    66 =>
        array(
            'Country' => 'British Virgin Islands',
            'Slug' => 'british-virgin-islands',
            'ISO2' => 'VG',
        ),
    67 =>
        array(
            'Country' => 'French Southern Territories',
            'Slug' => 'french-southern-territories',
            'ISO2' => 'TF',
        ),
    68 =>
        array(
            'Country' => 'Pitcairn',
            'Slug' => 'pitcairn',
            'ISO2' => 'PN',
        ),
    69 =>
        array(
            'Country' => 'Uruguay',
            'Slug' => 'uruguay',
            'ISO2' => 'UY',
        ),
    70 =>
        array(
            'Country' => 'Kenya',
            'Slug' => 'kenya',
            'ISO2' => 'KE',
        ),
    71 =>
        array(
            'Country' => 'Lebanon',
            'Slug' => 'lebanon',
            'ISO2' => 'LB',
        ),
    72 =>
        array(
            'Country' => 'Australia',
            'Slug' => 'australia',
            'ISO2' => 'AU',
        ),
    73 =>
        array(
            'Country' => 'Barbados',
            'Slug' => 'barbados',
            'ISO2' => 'BB',
        ),
    74 =>
        array(
            'Country' => 'San Marino',
            'Slug' => 'san-marino',
            'ISO2' => 'SM',
        ),
    75 =>
        array(
            'Country' => 'Niue',
            'Slug' => 'niue',
            'ISO2' => 'NU',
        ),
    76 =>
        array(
            'Country' => 'Seychelles',
            'Slug' => 'seychelles',
            'ISO2' => 'SC',
        ),
    77 =>
        array(
            'Country' => 'Somalia',
            'Slug' => 'somalia',
            'ISO2' => 'SO',
        ),
    78 =>
        array(
            'Country' => 'Botswana',
            'Slug' => 'botswana',
            'ISO2' => 'BW',
        ),
    79 =>
        array(
            'Country' => 'Albania',
            'Slug' => 'albania',
            'ISO2' => 'AL',
        ),
    80 =>
        array(
            'Country' => 'Senegal',
            'Slug' => 'senegal',
            'ISO2' => 'SN',
        ),
    81 =>
        array(
            'Country' => 'Ukraine',
            'Slug' => 'ukraine',
            'ISO2' => 'UA',
        ),
    82 =>
        array(
            'Country' => 'Djibouti',
            'Slug' => 'djibouti',
            'ISO2' => 'DJ',
        ),
    83 =>
        array(
            'Country' => 'Egypt',
            'Slug' => 'egypt',
            'ISO2' => 'EG',
        ),
    84 =>
        array(
            'Country' => 'Thailand',
            'Slug' => 'thailand',
            'ISO2' => 'TH',
        ),
    85 =>
        array(
            'Country' => 'Bhutan',
            'Slug' => 'bhutan',
            'ISO2' => 'BT',
        ),
    86 =>
        array(
            'Country' => 'Israel',
            'Slug' => 'israel',
            'ISO2' => 'IL',
        ),
    87 =>
        array(
            'Country' => 'Lithuania',
            'Slug' => 'lithuania',
            'ISO2' => 'LT',
        ),
    88 =>
        array(
            'Country' => 'Malaysia',
            'Slug' => 'malaysia',
            'ISO2' => 'MY',
        ),
    89 =>
        array(
            'Country' => 'Timor-Leste',
            'Slug' => 'timor-leste',
            'ISO2' => 'TL',
        ),
    90 =>
        array(
            'Country' => 'Belize',
            'Slug' => 'belize',
            'ISO2' => 'BZ',
        ),
    91 =>
        array(
            'Country' => 'Togo',
            'Slug' => 'togo',
            'ISO2' => 'TG',
        ),
    92 =>
        array(
            'Country' => 'New Caledonia',
            'Slug' => 'new-caledonia',
            'ISO2' => 'NC',
        ),
    93 =>
        array(
            'Country' => 'Tokelau',
            'Slug' => 'tokelau',
            'ISO2' => 'TK',
        ),
    94 =>
        array(
            'Country' => 'Tuvalu',
            'Slug' => 'tuvalu',
            'ISO2' => 'TV',
        ),
    95 =>
        array(
            'Country' => 'Vanuatu',
            'Slug' => 'vanuatu',
            'ISO2' => 'VU',
        ),
    96 =>
        array(
            'Country' => 'Anguilla',
            'Slug' => 'anguilla',
            'ISO2' => 'AI',
        ),
    97 =>
        array(
            'Country' => 'Netherlands',
            'Slug' => 'netherlands',
            'ISO2' => 'NL',
        ),
    98 =>
        array(
            'Country' => 'Mayotte',
            'Slug' => 'mayotte',
            'ISO2' => 'YT',
        ),
    99 =>
        array(
            'Country' => 'Brazil',
            'Slug' => 'brazil',
            'ISO2' => 'BR',
        ),
    100 =>
        array(
            'Country' => 'Dominican Republic',
            'Slug' => 'dominican-republic',
            'ISO2' => 'DO',
        ),
    101 =>
        array(
            'Country' => 'Haiti',
            'Slug' => 'haiti',
            'ISO2' => 'HT',
        ),
    102 =>
        array(
            'Country' => 'Congo (Kinshasa)',
            'Slug' => 'congo-kinshasa',
            'ISO2' => 'CD',
        ),
    103 =>
        array(
            'Country' => 'Czech Republic',
            'Slug' => 'czech-republic',
            'ISO2' => 'CZ',
        ),
    104 =>
        array(
            'Country' => 'Jersey',
            'Slug' => 'jersey',
            'ISO2' => 'JE',
        ),
    105 =>
        array(
            'Country' => 'American Samoa',
            'Slug' => 'american-samoa',
            'ISO2' => 'AS',
        ),
    106 =>
        array(
            'Country' => 'Canada',
            'Slug' => 'canada',
            'ISO2' => 'CA',
        ),
    107 =>
        array(
            'Country' => 'Nepal',
            'Slug' => 'nepal',
            'ISO2' => 'NP',
        ),
    108 =>
        array(
            'Country' => 'Russian Federation',
            'Slug' => 'russia',
            'ISO2' => 'RU',
        ),
    109 =>
        array(
            'Country' => 'Gambia',
            'Slug' => 'gambia',
            'ISO2' => 'GM',
        ),
    110 =>
        array(
            'Country' => 'Mexico',
            'Slug' => 'mexico',
            'ISO2' => 'MX',
        ),
    111 =>
        array(
            'Country' => 'Micronesia, Federated States of',
            'Slug' => 'micronesia',
            'ISO2' => 'FM',
        ),
    112 =>
        array(
            'Country' => 'Turks and Caicos Islands',
            'Slug' => 'turks-and-caicos-islands',
            'ISO2' => 'TC',
        ),
    113 =>
        array(
            'Country' => 'Norfolk Island',
            'Slug' => 'norfolk-island',
            'ISO2' => 'NF',
        ),
    114 =>
        array(
            'Country' => 'Angola',
            'Slug' => 'angola',
            'ISO2' => 'AO',
        ),
    115 =>
        array(
            'Country' => 'Bouvet Island',
            'Slug' => 'bouvet-island',
            'ISO2' => 'BV',
        ),
    116 =>
        array(
            'Country' => 'Turkey',
            'Slug' => 'turkey',
            'ISO2' => 'TR',
        ),
    117 =>
        array(
            'Country' => 'Equatorial Guinea',
            'Slug' => 'equatorial-guinea',
            'ISO2' => 'GQ',
        ),
    118 =>
        array(
            'Country' => 'Italy',
            'Slug' => 'italy',
            'ISO2' => 'IT',
        ),
    119 =>
        array(
            'Country' => 'Namibia',
            'Slug' => 'namibia',
            'ISO2' => 'NA',
        ),
    120 =>
        array(
            'Country' => 'Swaziland',
            'Slug' => 'swaziland',
            'ISO2' => 'SZ',
        ),
    121 =>
        array(
            'Country' => 'Bolivia',
            'Slug' => 'bolivia',
            'ISO2' => 'BO',
        ),
    122 =>
        array(
            'Country' => 'Libya',
            'Slug' => 'libya',
            'ISO2' => 'LY',
        ),
    123 =>
        array(
            'Country' => 'Mali',
            'Slug' => 'mali',
            'ISO2' => 'ML',
        ),
    124 =>
        array(
            'Country' => 'Kyrgyzstan',
            'Slug' => 'kyrgyzstan',
            'ISO2' => 'KG',
        ),
    125 =>
        array(
            'Country' => 'Saint-Barthélemy',
            'Slug' => 'saint-barthélemy',
            'ISO2' => 'BL',
        ),
    126 =>
        array(
            'Country' => 'Saint-Martin (French part)',
            'Slug' => 'saint-martin-french-part',
            'ISO2' => 'MF',
        ),
    127 =>
        array(
            'Country' => 'Tajikistan',
            'Slug' => 'tajikistan',
            'ISO2' => 'TJ',
        ),
    128 =>
        array(
            'Country' => 'Central African Republic',
            'Slug' => 'central-african-republic',
            'ISO2' => 'CF',
        ),
    129 =>
        array(
            'Country' => 'Congo (Brazzaville)',
            'Slug' => 'congo-brazzaville',
            'ISO2' => 'CG',
        ),
    130 =>
        array(
            'Country' => 'New Zealand',
            'Slug' => 'new-zealand',
            'ISO2' => 'NZ',
        ),
    131 =>
        array(
            'Country' => 'Myanmar',
            'Slug' => 'myanmar',
            'ISO2' => 'MM',
        ),
    132 =>
        array(
            'Country' => 'Uganda',
            'Slug' => 'uganda',
            'ISO2' => 'UG',
        ),
    133 =>
        array(
            'Country' => 'United Kingdom',
            'Slug' => 'united-kingdom',
            'ISO2' => 'GB',
        ),
    134 =>
        array(
            'Country' => 'Belarus',
            'Slug' => 'belarus',
            'ISO2' => 'BY',
        ),
    135 =>
        array(
            'Country' => 'El Salvador',
            'Slug' => 'el-salvador',
            'ISO2' => 'SV',
        ),
    136 =>
        array(
            'Country' => 'Spain',
            'Slug' => 'spain',
            'ISO2' => 'ES',
        ),
    137 =>
        array(
            'Country' => 'Denmark',
            'Slug' => 'denmark',
            'ISO2' => 'DK',
        ),
    138 =>
        array(
            'Country' => 'Mozambique',
            'Slug' => 'mozambique',
            'ISO2' => 'MZ',
        ),
    139 =>
        array(
            'Country' => 'Pakistan',
            'Slug' => 'pakistan',
            'ISO2' => 'PK',
        ),
    140 =>
        array(
            'Country' => 'Morocco',
            'Slug' => 'morocco',
            'ISO2' => 'MA',
        ),
    141 =>
        array(
            'Country' => 'Tunisia',
            'Slug' => 'tunisia',
            'ISO2' => 'TN',
        ),
    142 =>
        array(
            'Country' => 'Palau',
            'Slug' => 'palau',
            'ISO2' => 'PW',
        ),
    143 =>
        array(
            'Country' => 'Portugal',
            'Slug' => 'portugal',
            'ISO2' => 'PT',
        ),
    144 =>
        array(
            'Country' => 'Venezuela (Bolivarian Republic)',
            'Slug' => 'venezuela',
            'ISO2' => 'VE',
        ),
    145 =>
        array(
            'Country' => 'Iran, Islamic Republic of',
            'Slug' => 'iran',
            'ISO2' => 'IR',
        ),
    146 =>
        array(
            'Country' => 'Mauritius',
            'Slug' => 'mauritius',
            'ISO2' => 'MU',
        ),
    147 =>
        array(
            'Country' => 'Algeria',
            'Slug' => 'algeria',
            'ISO2' => 'DZ',
        ),
    148 =>
        array(
            'Country' => 'Mauritania',
            'Slug' => 'mauritania',
            'ISO2' => 'MR',
        ),
    149 =>
        array(
            'Country' => 'Cocos (Keeling) Islands',
            'Slug' => 'cocos-keeling-islands',
            'ISO2' => 'CC',
        ),
    150 =>
        array(
            'Country' => 'Cyprus',
            'Slug' => 'cyprus',
            'ISO2' => 'CY',
        ),
    151 =>
        array(
            'Country' => 'Guatemala',
            'Slug' => 'guatemala',
            'ISO2' => 'GT',
        ),
    152 =>
        array(
            'Country' => 'Macao, SAR China',
            'Slug' => 'macao-sar-china',
            'ISO2' => 'MO',
        ),
    153 =>
        array(
            'Country' => 'Rwanda',
            'Slug' => 'rwanda',
            'ISO2' => 'RW',
        ),
    154 =>
        array(
            'Country' => 'Antigua and Barbuda',
            'Slug' => 'antigua-and-barbuda',
            'ISO2' => 'AG',
        ),
    155 =>
        array(
            'Country' => 'Bosnia and Herzegovina',
            'Slug' => 'bosnia-and-herzegovina',
            'ISO2' => 'BA',
        ),
    156 =>
        array(
            'Country' => 'Iraq',
            'Slug' => 'iraq',
            'ISO2' => 'IQ',
        ),
    157 =>
        array(
            'Country' => 'Lesotho',
            'Slug' => 'lesotho',
            'ISO2' => 'LS',
        ),
    158 =>
        array(
            'Country' => 'Northern Mariana Islands',
            'Slug' => 'northern-mariana-islands',
            'ISO2' => 'MP',
        ),
    159 =>
        array(
            'Country' => 'Saint Helena',
            'Slug' => 'saint-helena',
            'ISO2' => 'SH',
        ),
    160 =>
        array(
            'Country' => 'Costa Rica',
            'Slug' => 'costa-rica',
            'ISO2' => 'CR',
        ),
    161 =>
        array(
            'Country' => 'Isle of Man',
            'Slug' => 'isle-of-man',
            'ISO2' => 'IM',
        ),
    162 =>
        array(
            'Country' => 'Lao PDR',
            'Slug' => 'lao-pdr',
            'ISO2' => 'LA',
        ),
    163 =>
        array(
            'Country' => 'Norway',
            'Slug' => 'norway',
            'ISO2' => 'NO',
        ),
    164 =>
        array(
            'Country' => 'Argentina',
            'Slug' => 'argentina',
            'ISO2' => 'AR',
        ),
    165 =>
        array(
            'Country' => 'Croatia',
            'Slug' => 'croatia',
            'ISO2' => 'HR',
        ),
    166 =>
        array(
            'Country' => 'Ethiopia',
            'Slug' => 'ethiopia',
            'ISO2' => 'ET',
        ),
    167 =>
        array(
            'Country' => 'Korea (South)',
            'Slug' => 'korea-south',
            'ISO2' => 'KR',
        ),
    168 =>
        array(
            'Country' => 'Macedonia, Republic of',
            'Slug' => 'macedonia',
            'ISO2' => 'MK',
        ),
    169 =>
        array(
            'Country' => 'Papua New Guinea',
            'Slug' => 'papua-new-guinea',
            'ISO2' => 'PG',
        ),
    170 =>
        array(
            'Country' => 'Germany',
            'Slug' => 'germany',
            'ISO2' => 'DE',
        ),
    171 =>
        array(
            'Country' => 'Liberia',
            'Slug' => 'liberia',
            'ISO2' => 'LR',
        ),
    172 =>
        array(
            'Country' => 'Sweden',
            'Slug' => 'sweden',
            'ISO2' => 'SE',
        ),
    173 =>
        array(
            'Country' => 'Ireland',
            'Slug' => 'ireland',
            'ISO2' => 'IE',
        ),
    174 =>
        array(
            'Country' => 'Zimbabwe',
            'Slug' => 'zimbabwe',
            'ISO2' => 'ZW',
        ),
    175 =>
        array(
            'Country' => 'Antarctica',
            'Slug' => 'antarctica',
            'ISO2' => 'AQ',
        ),
    176 =>
        array(
            'Country' => 'Chad',
            'Slug' => 'chad',
            'ISO2' => 'TD',
        ),
    177 =>
        array(
            'Country' => 'Serbia',
            'Slug' => 'serbia',
            'ISO2' => 'RS',
        ),
    178 =>
        array(
            'Country' => 'South Georgia and the South Sandwich Islands',
            'Slug' => 'south-georgia-and-the-south-sandwich-islands',
            'ISO2' => 'GS',
        ),
    179 =>
        array(
            'Country' => 'Yemen',
            'Slug' => 'yemen',
            'ISO2' => 'YE',
        ),
    180 =>
        array(
            'Country' => 'Cayman Islands',
            'Slug' => 'cayman-islands',
            'ISO2' => 'KY',
        ),
    181 =>
        array(
            'Country' => 'Nicaragua',
            'Slug' => 'nicaragua',
            'ISO2' => 'NI',
        ),
    182 =>
        array(
            'Country' => 'Aruba',
            'Slug' => 'aruba',
            'ISO2' => 'AW',
        ),
    183 =>
        array(
            'Country' => 'Cameroon',
            'Slug' => 'cameroon',
            'ISO2' => 'CM',
        ),
    184 =>
        array(
            'Country' => 'Slovakia',
            'Slug' => 'slovakia',
            'ISO2' => 'SK',
        ),
    185 =>
        array(
            'Country' => 'Honduras',
            'Slug' => 'honduras',
            'ISO2' => 'HN',
        ),
    186 =>
        array(
            'Country' => 'Marshall Islands',
            'Slug' => 'marshall-islands',
            'ISO2' => 'MH',
        ),
    187 =>
        array(
            'Country' => 'Montserrat',
            'Slug' => 'montserrat',
            'ISO2' => 'MS',
        ),
    188 =>
        array(
            'Country' => 'Andorra',
            'Slug' => 'andorra',
            'ISO2' => 'AD',
        ),
    189 =>
        array(
            'Country' => 'Benin',
            'Slug' => 'benin',
            'ISO2' => 'BJ',
        ),
    190 =>
        array(
            'Country' => 'Mongolia',
            'Slug' => 'mongolia',
            'ISO2' => 'MN',
        ),
    191 =>
        array(
            'Country' => 'Guernsey',
            'Slug' => 'guernsey',
            'ISO2' => 'GG',
        ),
    192 =>
        array(
            'Country' => 'Nigeria',
            'Slug' => 'nigeria',
            'ISO2' => 'NG',
        ),
    193 =>
        array(
            'Country' => 'Zambia',
            'Slug' => 'zambia',
            'ISO2' => 'ZM',
        ),
    194 =>
        array(
            'Country' => 'Saint Lucia',
            'Slug' => 'saint-lucia',
            'ISO2' => 'LC',
        ),
    195 =>
        array(
            'Country' => 'Burkina Faso',
            'Slug' => 'burkina-faso',
            'ISO2' => 'BF',
        ),
    196 =>
        array(
            'Country' => 'Paraguay',
            'Slug' => 'paraguay',
            'ISO2' => 'PY',
        ),
    197 =>
        array(
            'Country' => 'Uzbekistan',
            'Slug' => 'uzbekistan',
            'ISO2' => 'UZ',
        ),
    198 =>
        array(
            'Country' => 'Estonia',
            'Slug' => 'estonia',
            'ISO2' => 'EE',
        ),
    199 =>
        array(
            'Country' => 'Liechtenstein',
            'Slug' => 'liechtenstein',
            'ISO2' => 'LI',
        ),
    200 =>
        array(
            'Country' => 'Oman',
            'Slug' => 'oman',
            'ISO2' => 'OM',
        ),
    201 =>
        array(
            'Country' => 'Malta',
            'Slug' => 'malta',
            'ISO2' => 'MT',
        ),
    202 =>
        array(
            'Country' => 'Nauru',
            'Slug' => 'nauru',
            'ISO2' => 'NR',
        ),
    203 =>
        array(
            'Country' => 'South Sudan',
            'Slug' => 'south-sudan',
            'ISO2' => 'SS',
        ),
    204 =>
        array(
            'Country' => 'Sri Lanka',
            'Slug' => 'sri-lanka',
            'ISO2' => 'LK',
        ),
    205 =>
        array(
            'Country' => 'United Arab Emirates',
            'Slug' => 'united-arab-emirates',
            'ISO2' => 'AE',
        ),
    206 =>
        array(
            'Country' => 'Jamaica',
            'Slug' => 'jamaica',
            'ISO2' => 'JM',
        ),
    207 =>
        array(
            'Country' => 'Kazakhstan',
            'Slug' => 'kazakhstan',
            'ISO2' => 'KZ',
        ),
    208 =>
        array(
            'Country' => 'Holy See (Vatican City State)',
            'Slug' => 'holy-see-vatican-city-state',
            'ISO2' => 'VA',
        ),
    209 =>
        array(
            'Country' => 'Kuwait',
            'Slug' => 'kuwait',
            'ISO2' => 'KW',
        ),
    210 =>
        array(
            'Country' => 'Philippines',
            'Slug' => 'philippines',
            'ISO2' => 'PH',
        ),
    211 =>
        array(
            'Country' => 'Saint Vincent and Grenadines',
            'Slug' => 'saint-vincent-and-the-grenadines',
            'ISO2' => 'VC',
        ),
    212 =>
        array(
            'Country' => 'Svalbard and Jan Mayen Islands',
            'Slug' => 'svalbard-and-jan-mayen-islands',
            'ISO2' => 'SJ',
        ),
    213 =>
        array(
            'Country' => 'Belgium',
            'Slug' => 'belgium',
            'ISO2' => 'BE',
        ),
    214 =>
        array(
            'Country' => 'Christmas Island',
            'Slug' => 'christmas-island',
            'ISO2' => 'CX',
        ),
    215 =>
        array(
            'Country' => 'Eritrea',
            'Slug' => 'eritrea',
            'ISO2' => 'ER',
        ),
    216 =>
        array(
            'Country' => 'Suriname',
            'Slug' => 'suriname',
            'ISO2' => 'SR',
        ),
    217 =>
        array(
            'Country' => 'Hong Kong, SAR China',
            'Slug' => 'hong-kong-sar-china',
            'ISO2' => 'HK',
        ),
    218 =>
        array(
            'Country' => 'Palestinian Territory',
            'Slug' => 'palestine',
            'ISO2' => 'PS',
        ),
    219 =>
        array(
            'Country' => 'Bangladesh',
            'Slug' => 'bangladesh',
            'ISO2' => 'BD',
        ),
    220 =>
        array(
            'Country' => 'Qatar',
            'Slug' => 'qatar',
            'ISO2' => 'QA',
        ),
    221 =>
        array(
            'Country' => 'French Polynesia',
            'Slug' => 'french-polynesia',
            'ISO2' => 'PF',
        ),
    222 =>
        array(
            'Country' => 'Jordan',
            'Slug' => 'jordan',
            'ISO2' => 'JO',
        ),
    223 =>
        array(
            'Country' => 'Niger',
            'Slug' => 'niger',
            'ISO2' => 'NE',
        ),
    224 =>
        array(
            'Country' => 'Réunion',
            'Slug' => 'réunion',
            'ISO2' => 'RE',
        ),
    225 =>
        array(
            'Country' => 'Trinidad and Tobago',
            'Slug' => 'trinidad-and-tobago',
            'ISO2' => 'TT',
        ),
    226 =>
        array(
            'Country' => 'Virgin Islands, US',
            'Slug' => 'virgin-islands',
            'ISO2' => 'VI',
        ),
    227 =>
        array(
            'Country' => 'British Indian Ocean Territory',
            'Slug' => 'british-indian-ocean-territory',
            'ISO2' => 'IO',
        ),
    228 =>
        array(
            'Country' => 'Greenland',
            'Slug' => 'greenland',
            'ISO2' => 'GL',
        ),
    229 =>
        array(
            'Country' => 'Cambodia',
            'Slug' => 'cambodia',
            'ISO2' => 'KH',
        ),
    230 =>
        array(
            'Country' => 'Fiji',
            'Slug' => 'fiji',
            'ISO2' => 'FJ',
        ),
    231 =>
        array(
            'Country' => 'Turkmenistan',
            'Slug' => 'turkmenistan',
            'ISO2' => 'TM',
        ),
    232 =>
        array(
            'Country' => 'Bahamas',
            'Slug' => 'bahamas',
            'ISO2' => 'BS',
        ),
    233 =>
        array(
            'Country' => 'Azerbaijan',
            'Slug' => 'azerbaijan',
            'ISO2' => 'AZ',
        ),
    234 =>
        array(
            'Country' => 'Bulgaria',
            'Slug' => 'bulgaria',
            'ISO2' => 'BG',
        ),
    235 =>
        array(
            'Country' => 'Montenegro',
            'Slug' => 'montenegro',
            'ISO2' => 'ME',
        ),
    236 =>
        array(
            'Country' => 'Romania',
            'Slug' => 'romania',
            'ISO2' => 'RO',
        ),
    237 =>
        array(
            'Country' => 'Brunei Darussalam',
            'Slug' => 'brunei',
            'ISO2' => 'BN',
        ),
    238 =>
        array(
            'Country' => 'Georgia',
            'Slug' => 'georgia',
            'ISO2' => 'GE',
        ),
    239 =>
        array(
            'Country' => 'Indonesia',
            'Slug' => 'indonesia',
            'ISO2' => 'ID',
        ),
    240 =>
        array(
            'Country' => 'Saudi Arabia',
            'Slug' => 'saudi-arabia',
            'ISO2' => 'SA',
        ),
    241 =>
        array(
            'Country' => 'South Africa',
            'Slug' => 'south-africa',
            'ISO2' => 'ZA',
        ),
    242 =>
        array(
            'Country' => 'Burundi',
            'Slug' => 'burundi',
            'ISO2' => 'BI',
        ),
    243 =>
        array(
            'Country' => 'Luxembourg',
            'Slug' => 'luxembourg',
            'ISO2' => 'LU',
        ),
    244 =>
        array(
            'Country' => 'Tanzania, United Republic of',
            'Slug' => 'tanzania',
            'ISO2' => 'TZ',
        ),
    245 =>
        array(
            'Country' => 'US Minor Outlying Islands',
            'Slug' => 'us-minor-outlying-islands',
            'ISO2' => 'UM',
        ),
    246 =>
        array(
            'Country' => 'Guam',
            'Slug' => 'guam',
            'ISO2' => 'GU',
        ),
    247 =>
        array(
            'Country' => 'Moldova',
            'Slug' => 'moldova',
            'ISO2' => 'MD',
        ),
);
