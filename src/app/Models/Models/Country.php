<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'name',
        'alpha2Code',
        'alpha3Code',
        'slug',
        'callingCodes',
        'capital',
        'altSpellings',
        'region',
        'subregion',
        'population',
        'latitude',
        'longitude',
        'demonym',
        'area',
        'gini',
        'timezones',
        'borders',
        'nativeName',
        'numericCode',
        'currencies',
        'languages',
        'translations',
        'flag',
        'regionalBlocs',
        'cioc'
    ];

    protected $casts = [
        'callingCodes' => 'array',
        'altSpellings' => 'array',
        'timezones' => 'array',
        'borders' => 'array',
        'currencies' => 'array',
        'languages' => 'array',
        'translations' => 'array',
        'regionalBlocs' => 'array',
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}
