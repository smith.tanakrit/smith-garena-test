<?php

namespace App\Http\Controllers;

use App\Library\GenerateCountryPDF;
use App\Models\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ExportController extends Controller
{
    public function exportAll(Request $request)
    {
        $filePath = storage_path('app/public/covid-data-pdf/world/covid-data-world.pdf');

        if (!File::exists($filePath)) {
            return response()->json(['message' => 'NO_CONTENT']);
        }

        return response()->download($filePath);
    }

    public function exportBySlug(Request $request, $slug)
    {
        $country = Country::query()
            ->where('slug', '=', $slug)
            ->first();

        if (!$country) {
            return response()->json(['message' => 'NOT_FOUND'], 404);
        }

        return self::export([$country], optional($country)->slug . '-' . now()->format('Y-m-d'));
    }

    public function export(array $countries, string $fileName = 'covid-country-data')
    {
        $pdf = new GenerateCountryPDF($countries);

        if ($content = $pdf->generatePDF()) {
            return $content->download("{$fileName}.pdf");
        }

        return response()->json(['message' => 'NO_CONTENT']);
    }

    public function country(Request $request)
    {
        $countries = Country::query()
            ->where('population', '>=', 300000)
            ->take(29)
            ->get();

        if ($countries->isEmpty()) {
            return response()->json([
                'message' => 'NOT_FOUND',
                'data' => [],
            ]);
        }

        return response()->json([
            'message' => 'OK',
            'data' => $countries,
        ]);
    }

    public function countryBySlug(Request $request, $slug)
    {
        $country = Country::query()->where('slug', '=', $slug)->first();

        if (!$country) {
            return response()->json([
                'message' => 'NOT_FOUND',
                'data' => [],
            ]);
        }

        return response()->json([
            'message' => 'OK',
            'data' => $country,
        ]);
    }
}
