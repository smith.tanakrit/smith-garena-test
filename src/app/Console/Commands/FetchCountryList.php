<?php

namespace App\Console\Commands;

use App\Library\GenerateCountryPDF;
use App\Models\Models\Country;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;

class FetchCountryList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch-country-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch country list from API to database.';

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    private $sourceCountryApi = 'https://restcountries.eu/rest/v2/all';

    /**
     * @var string
     */
    private $covidCountryApi = 'https://api.covid19api.com/countries';

    /**
     * @var string
     */
    private $pdfFolderPath = 'public/covid-data-pdf';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client([
            'verify' => false,
            'timeout' => 10,
        ]);

        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $countries = self::requestApi($this->sourceCountryApi);
        $covidCountry = self::requestApi($this->covidCountryApi) ?: config('covid_countries');
        $totalCountries = count($countries);

        if (!empty($countries)) {
            $this->getOutput()->progressStart($totalCountries);

            foreach ($countries as $country) {
                if (self::isValidCountryData($country) === false) {
                    continue;
                }

                try {
                    $country['slug'] = self::mapCountrySlug($country, $covidCountry);
                    $credential = self::countryCredential($country);

                    Country::query()
                        ->updateOrCreate(
                            Arr::only(
                                $credential,
                                [
                                    'name',
                                    'alpha2Code',
                                    'alpha3Code',
                                    'slug',
                                ]
                            ),
                            $credential
                        );
                } catch (\Exception $exception) {
                    $this->error('Something went wrong.');
                }

                $this->getOutput()->progressAdvance();
            }

            $this->getOutput()->progressFinish();
        } else {
            $this->info('Not found any country.');
        }

        self::generatePDF();
        self::generateEachCountry();

        $this->info("Finished, {$totalCountries} countries has been updated.");
    }

    /**
     * @param $country
     * @param array $covidCountry
     * @return string
     */
    private function mapCountrySlug($country, $covidCountry = [])
    {
        $slug = Str::slug(Arr::get($country, 'name'));
        $mapByCode = collect($covidCountry)
            ->filter(function ($value) use ($country) {
                return strtoupper(Arr::get($value, 'ISO2')) === strtoupper(Arr::get($country, 'alpha2Code'));
            })
            ->first();

        if ($mapByCode) {
            $slug = Arr::get($mapByCode, 'Slug');
        }

        return $slug;
    }

    /**
     * @param string $url
     * @return array|mixed
     */
    private function requestApi(string $url)
    {
        try {
            $content = self::makeGuzzelRequest($url)
                ->getBody()
                ->getContents();
            $data = json_decode($content, true);
        } catch (GuzzleException $e) {
            $data = [];
            $this->error("Unable to call API : {$url}");
        }

        return $data;
    }

    /**
     * @param $url
     * @param string $method
     * @param array $option
     * @return ResponseInterface
     * @throws GuzzleException
     */
    private function makeGuzzelRequest($url, $method = 'GET', $option = [])
    {
        return $this->client->request($method, $url, $option);
    }

    /**
     * @param $data
     * @return bool
     */
    private function isValidCountryData($data)
    {
        return filled(Arr::get($data, 'name'))
            && filled(Arr::get($data, 'alpha2Code'))
            && filled(Arr::get($data, 'alpha3Code'));
    }

    /**
     * @param $country
     * @return array
     */
    private function countryCredential($country)
    {
        $latlng = Arr::get($country, 'latlng');
        $country['latitude'] = Arr::first($latlng);
        $country['longitude'] = Arr::last($latlng);

        return Arr::only($country, [
            'name',
            'alpha2Code',
            'alpha3Code',
            'slug',
            'callingCodes',
            'capital',
            'altSpellings',
            'region',
            'subregion',
            'population',
            'latitude',
            'longitude',
            'demonym',
            'area',
            'gini',
            'timezones',
            'borders',
            'nativeName',
            'numericCode',
            'currencies',
            'languages',
            'translations',
            'flag',
            'regionalBlocs',
            'cioc',
        ]);
    }

    /**
     * generatePDF
     */
    private function generatePDF()
    {
        $countries = Country::query()->get();

        if ($countries->isEmpty()) {
            $this->info('No data to generate');
            return;
        }

        $this->info('Generate World PDF in progress.');

        $pdf = new GenerateCountryPDF($countries->toArray());

        self::savePDF($pdf, 'world');

        $this->info('Generate World PDF completed.');
    }

    /**
     * Generate each country separate file.
     */
    private function generateEachCountry()
    {
        $countries = Country::query();
        $total = $countries->count();

        if ($total <= 0) {
            $this->info('No data to generate');
            return;
        }

        $this->info('Generate each country PDF in progress.');
        $this->getOutput()->progressStart($total);

        $countries->cursor()->each(function ($country) {
            $pdf = new GenerateCountryPDF([$country]);

            self::savePDF($pdf, optional($country)->slug);

            $this->getOutput()->progressAdvance();
        });

        $this->getOutput()->progressFinish();
        $this->info('Generate each country PDF completed.');
    }

    /**
     * @param GenerateCountryPDF $pdf
     * @param $folder
     * @param $slug
     */
    private function savePDF(GenerateCountryPDF $pdf, $slug)
    {
        if ($content = $pdf->generatePDF()) {
            $folder = "{$this->pdfFolderPath}/{$slug}";
            $file = $content->download()->getOriginalContent();
            File::deleteDirectory(storage_path("app/{$folder}"));
            Storage::put("{$folder}/covid-data-{$slug}.pdf", $file) ;
        }
    }
}
