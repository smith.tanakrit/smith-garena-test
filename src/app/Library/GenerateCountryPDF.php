<?php

namespace App\Library;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;
use PDF;

class GenerateCountryPDF
{
    /**
     * @var array
     */
    protected $country;

    /**
     * @var array|mixed
     */
    protected $parameters;

    /**
     * @var Client
     */
    private $client;

    /**
     * GenerateCountryPDF constructor.
     * @param array $country
     * @param array $parameters
     */
    public function __construct(array $country, array $parameters = [])
    {
        $this->country = $country;
        $this->client = new Client([
            'verify' => false,
            'timeout' => 10,
        ]);

        if (empty($parameters)) {
            $parameters = $this->getDefaultParameters();
        }

        $this->parameters = $parameters;
    }

    public function generatePDF()
    {
        $pdf = null;

        if (!empty($this->country)) {
            foreach ($this->country as &$country) {
                $covidResponse = self::requestCovidApi(data_get($country, 'slug'), $this->parameters);
                $country['covid_data'] = collect($covidResponse)
                    ->groupBy(function ($data) {
                        return Carbon::parse(Arr::get($data, 'Date'))->month;
                    })
                    ->map(function ($value) {
                        return $value->max('Cases');
                    });
            }

            $pdf = PDF::loadView('covid_country', ['countries' => $this->country]);
        }

        return $pdf;
    }

    /**
     * @return array
     */
    private function getDefaultParameters()
    {
        return [
            'from' => Carbon::create(2020, 01)->startOfMonth()->toISOString(),
            'to' => Carbon::create(2020, 05)->endOfMonth()->toISOString(),
        ];
    }

    /**
     * @param string $country
     * @param array $option
     * @return array|mixed
     */
    private function requestCovidApi(string $country, $option = [])
    {
        $url = "https://api.covid19api.com/total/country/{$country}/status/confirmed";

        if (!empty($option)) {
            $url .= '?' . http_build_query($option);
        }

        try {
            $content = self::makeRequest($url)->getBody()->getContents();

            $data = json_decode($content, true);
        } catch (GuzzleException $e) {
            $data = [];
        }

        return $data;
    }

    /**
     * @param $url
     * @param string $method
     * @param array $option
     * @return ResponseInterface
     * @throws GuzzleException
     */
    private function makeRequest($url, $method = 'GET', $option = [])
    {
        return $this->client->request($method, $url, $option);
    }
}
