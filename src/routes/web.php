<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix' => 'api',
    'as' => 'api.',
], function () {

    Route::get('countries', 'ExportController@country')->name('country');
    Route::get('countries/slug/{slug}', 'ExportController@countryBySlug')->name('country_by_slug');

    Route::group([
        'prefix' => 'exports/covid-countries',
        'as' => 'export.',
    ], function () {
        Route::get('all', 'ExportController@exportAll')->name('all');
        Route::get('world', 'ExportController@exportAll')->name('world');
        Route::get('{slug}', 'ExportController@exportBySlug')->name('by_slug');
    });
});
