<!DOCTYPE html>
<html lang="en">

<head>
  <title>Covid Country Detail</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style>
      @font-face {
          font-family: 'THSarabunNew';
          font-style: normal;
          font-weight: normal;
          src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
      }

      @font-face {
          font-family: 'THSarabunNew';
          font-style: normal;
          font-weight: bold;
          src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
      }

      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: normal;
          src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
      }

      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: bold;
          src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
      }

      body {
          font-family: "THSarabunNew";
      }

      @page {
          margin: 100px 50px;
      }

      .header {
          position: fixed;
          left: 0px;
          top: -100px;
          right: 0px;
          height: 100px;
          text-align: center;
      }

      .footer {
          position: fixed;
          left: 0px;
          bottom: -50px;
          right: 0px;
          height: 50px;
          text-align: center;
      }

      .footer .pagenum:before {
          content: counter(page);
      }

      .text-center {
          text-align: center
      }

      .text-right {
          text-align: right;
      }

      .text-left {
          text-align: left;
      }
  </style>
</head>

<body>
<div class="header" style="margin-bottom: 2rem;">
  <h2>บริษัท การีนา ออนไลน์ (ประเทศไทย) จำกัด 89 อาคารเอไอเอ แคปปิตอล เซ็นเตอร์ ชั้น 24 ถนนรัชดาภิเษก แขวงดินแดง
    เขตดินแดง กทม. 10400</h2>
</div>
<div class="footer">
  หน้า <span class="pagenum"></span>
</div>

<table border="1" style="width: 100%;margin-top: 1rem;">
  <tr>
    <th class="text-center">ชื่อประเทศ</th>
    <th class="text-center">รูปธง</th>
    <th class="text-center">อักษรย่อ</th>
    <th class="text-center">ภูมิภาค</th>
    <th class="text-center">สกุลเงิน</th>
    <th class="text-center">โซนเวลา</th>
    <th class="text-center">จำนวนการติดเชื้อในเดือน <br> (มกราคม-พฤษภาคม ของปี 2020)</th>
  </tr>
  @foreach ($countries ?: [] as $country)
    <tr>
      <td class="text-center">{{ data_get($country, 'name') ?: '' }}</td>
      <td class="text-center" width="65">
        <img width="64" src="{{ 'https://www.countryflags.io/'. data_get($country, 'alpha2Code') .'/flat/64.png' }}"/>
      </td>
      <td class="text-center">{{ data_get($country, 'alpha3Code') ?: '' }}</td>
      <td class="text-center">{{ data_get($country, 'region') ?: '' }}</td>
      <td class="text-center">
        {{ data_get($country, 'currencies.0.name') ?: '' }} ({{ data_get($country, 'currencies.0.code') ?: '' }})
      </td>
      <td class="text-center">
        {{ data_get($country, 'timezones.0') ?: '' }}
      </td>
      <td class="text-center">
        @if (data_get($country, 'covid_data')->isNotEmpty())
          <table border="1" style="width: 100%;">
            @foreach(data_get($country, 'covid_data') as $monthIndex => $data)
              <tr>
                <td class="text-center">{{ \Carbon\Carbon::create()->month($monthIndex)->locale('th')->translatedFormat('F') }}</td>
                <td class="text-center">{{ number_format($data ?: 0) }}</td>
              </tr>
            @endforeach
          </table>
        @else
          No data
        @endif
      </td>
    </tr>
  @endforeach
  <tr>
    <td colspan="7" class="text-center">ดึงข้อมูลล่าสุดเมื่อ : {{ now()->locale('th')->addYears(543)->translatedFormat('Y-F-d H:i:s') }}</td>
  </tr>
</table>

</body>
</html>
