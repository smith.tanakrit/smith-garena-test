import Vue from 'vue'
import Router from 'vue-router'
import LanternFestival from '@/components/LanternFestival'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'lantern_festival',
      component: LanternFestival
    }
  ]
})
